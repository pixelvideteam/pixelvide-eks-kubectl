# Pixelvide Pipelines Pipe: Pixelvide EKS Kubectl

This pipe simplifies the Release process of the Bitbucket Pipe or any Bitbucket Cloud project by abstracting away the k8s object apply,
and update process.

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
- pipe: pixelvideteam/pixelvide-eks-kubectl:0.6.0
  variables:
    AWS_ACCESS_KEY_ID: '<string>' # Optional if already defined in the context.
    AWS_SECRET_ACCESS_KEY: '<string>' # Optional if already defined in the context.
    AWS_DEFAULT_REGION: '<string>' # Optional if already defined in the context.
    AWS_ROLE_ARN: '<string>' # Optional if already defined in the context.
    DOCKER_ECR_REPO_URL: '<string>' # Optional if already defined in the context.
    DOCKER_IMAGE_NAME: '<string>' # Optional if already defined in the context.
    EKS_CLUSTER_NAME: '<string>' # Optional if already defined in the context.
    K8s_ENV: '<string>' # Optional if already defined in the context.
    K8s_NAMESPACE: '<string>' # Optional if already defined in the context.
```

## Variables

| Variable                    | Usage                                                |
| ----------------------------| ---------------------------------------------------- |
| AWS_ACCESS_KEY_ID (**)      | AWS access key id. |
| AWS_SECRET_ACCESS_KEY (**)  | AWS secret key. |
| AWS_DEFAULT_REGION (**)     | The AWS region code (`us-east-1`, `us-west-2`, etc.) of the region containing the AWS resource(s). For more information, see [Regions and Endpoints](https://docs.aws.amazon.com/general/latest/gr/rande.html) in the _Amazon Web Services General Reference_. |
| AWS_ROLE_ARN (**)           | The AWS role ARN if we want to push image to different AWS account. |
| DOCKER_ECR_REPO_URL (**)    | The url of docker public ECR. |
| DOCKER_IMAGE_NAME (**)      | The name of an image to be released. |

_(*) = required variable._
_(**) = required variable. If this variable is configured as a repository, account or environment variable, it doesn't need to be declared in the pipe as it will be taken from the context. It can still be overridden when using the pipe._

## Examples

### Basic example:

Example releasing a new version of the pipe:

```yaml
script:
  - pipe: pixelvideteam/pixelvide-eks-kubectl:0.6.0

```

## Support
If you’d like help with this pipe, or you have an issue or feature request, [let us know on Community][community].

If you’re reporting an issue, please include:

* the version of the pipe
* relevant logs and error messages
* steps to reproduce

## License
Copyright (c) 2020 Atlassian and others.
Apache 2.0 licensed, see [LICENSE.txt](LICENSE.txt) file.

[community]: ops@pixelvide.com