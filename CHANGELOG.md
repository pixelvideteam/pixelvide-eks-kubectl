# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 0.6.0

- minor: assume role feature added for corss account eks access

## 0.5.5

- patch: Example added

## 0.5.4

- patch: CI/CD pipe upgrade

## 0.5.3

- patch: const will update at the time of delete
- patch: docker URI bug fix

## 0.5.2

- patch: new context added

## 0.5.1

- patch: const update feature added

## 0.5.0

- minor: job and cron deploy feature added

## 0.4.1

- patch: ingress patch remove json path fixed

## 0.4.0

- minor: remove web feature added

## 0.3.0

- minor: Update Ingress feature added

## 0.2.1

- patch: Typecast bug fix

## 0.2.0

- minor: deploy_web commands added

## 0.1.1

- patch: DEFAULT Context added

## 0.1.0

- minor: EKS configure added

## 0.0.1

- patch: Initial commit
