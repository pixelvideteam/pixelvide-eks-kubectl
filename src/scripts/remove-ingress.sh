#!/bin/bash

OLD_HOST=$1
INGRESS_NAME=$2

INGRESS_INDEX=$(kubectl get ingress ${INGRESS_NAME} -n ifmists -o json | jq "[ .spec.rules[] | .host == \"$(echo ${OLD_HOST})\"] | index(true)")

sed -i s/INGRESS_INDEX/${INGRESS_INDEX,,}/g /usr/bin/scripts/ingress-patch-remove.json

kubectl patch ingress ${INGRESS_NAME} -n ifmists --type json -p "$(cat /usr/bin/scripts/ingress-patch-remove.json)"