#!/usr/bin/env bash
#
# Release to Bintray.
#
# Assume AWS Role
#
# Required globals:
#   AWS_ROLE_ARN
#
set -ex
profile_name=$1

temp_role=$(aws sts assume-role \
   --role-arn $AWS_ROLE_ARN \
   --role-session-name 'pixelvide-eks-kubectl')

export AWS_ACCESS_KEY_ID=$(echo $temp_role | jq -r .Credentials.AccessKeyId)
export AWS_SECRET_ACCESS_KEY=$(echo $temp_role | jq -r .Credentials.SecretAccessKey)
export AWS_SESSION_TOKEN=$(echo $temp_role | jq -r .Credentials.SessionToken)

aws configure set aws_access_key_id $AWS_ACCESS_KEY_ID --profile $profile_name
aws configure set aws_secret_access_key $AWS_SECRET_ACCESS_KEY --profile $profile_name
aws configure set aws_session_token $AWS_SESSION_TOKEN --profile $profile_name