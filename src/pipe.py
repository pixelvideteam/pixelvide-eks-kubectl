import os
import sys
import subprocess
import glob
import stat
import yaml

from bitbucket_pipes_toolkit import Pipe, get_logger

schema = {
    'AWS_ACCESS_KEY_ID': {'type': 'string', 'required': True},
    'AWS_SECRET_ACCESS_KEY': {'type': 'string', 'required': True},
    'AWS_DEFAULT_REGION': {'type': 'string', 'required': True},
    'AWS_ROLE_ARN': {'type': 'string', 'required': True},
    'DOCKER_ECR_REPO_URL': {'type': 'string', 'required': True},
    'DOCKER_IMAGE_NAME': {'type': 'string', 'required': True},
    'EKS_CLUSTER_NAME': {'type': 'string', 'required': True},
    'K8s_ENV': {'type': 'string', 'required': True},
    'K8s_NAMESPACE': {'type': 'string', 'required': True},
    'KUBECTL_COMMAND': {'type': 'string', 'required': True},
    'KUBECTL_APPLY_ARGS': {'type': 'string', 'required': False, 'default': '-f'},
    'KUBECTL_ARGS': {'type': 'list', 'required': False, 'default': []},
    'INGRESS_NAME': {'type': 'string', 'required': False, 'nullable': True},
    'DOMAIN_NAME': {'type': 'string', 'required': False, 'nullable': True},
    'SERVICE_NAME': {'type': 'string', 'required': False, 'nullable': True},
    'LABELS': {'type': 'list', 'required': False},
    'WITH_DEFAULT_LABELS': {'type': 'boolean', 'required': False, 'default': True},
    'DEBUG': {'type': 'boolean', 'required': False, 'default': False}
}

logger = get_logger()


class EKSDeployPipe(Pipe):
    def configure(self):
        self._script_path = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'scripts')
        self._allow_executable()

        cluster_name = self.get_variable("EKS_CLUSTER_NAME")
        role = self.get_variable('AWS_ROLE_ARN')
        cmd = f'aws eks update-kubeconfig --name={cluster_name}'.split()

        if role is not None:
            script = os.path.join(self._script_path, 'assume-role.sh')
            profile_name = "pixelvide-eks-kubectl"

            try:
                subprocess.run([script, profile_name], check=True, capture_output=True, text=True)
            except subprocess.CalledProcessError as exc:
                self.fail(f'{exc.output} {exc.stderr}')

            os.environ['AWS_PROFILE'] = profile_name
            cmd.append(f"--role-arn={role}")
            cmd.append(f"--profile={profile_name}")

        self.log_info(self.get_variable('DEBUG'))
        if self.get_variable('DEBUG'):
            cmd.append("--verbose")
        result = subprocess.run(cmd, stdout=sys.stdout)

        if result.returncode != 0:
            self.fail(f'Failed to update the kube config.')
        else:
            self.success(f'Successfully updated the kube config.')

    def _allow_executable(self):
        for path in glob.iglob(os.path.join(self._script_path, '*.sh')):
            os.chmod(path, os.stat(path).st_mode | stat.S_IEXEC)

    def update_constants(self, template):
        # Update Branch Name in templates
        bitbucket_branch = os.getenv('BITBUCKET_BRANCH').lower()

        cmd = f'sed -i s/BRANCH_NAME/{bitbucket_branch}/g {template}'.split()
        subprocess.run(cmd, stdout=sys.stdout)

        # Update Docker URI in templates
        docker_ecr_repo_url = self.get_variable('DOCKER_ECR_REPO_URL')
        docker_image_name = self.get_variable('DOCKER_IMAGE_NAME')
        bitbucket_commit = os.getenv('BITBUCKET_COMMIT')
        docker_image_url = f'{docker_ecr_repo_url}/{docker_image_name}:{bitbucket_commit}'

        cmd = f'sed -i s#DOCKER_IMAGE_URL#{docker_image_url}#g {template}'.split()
        subprocess.run(cmd, stdout=sys.stdout)

    def update_labels_in_metadata(self, template, labels):
        with open(template, 'r') as template_file:
            yamls = list(yaml.safe_load_all(template_file.read()))
            if None in yamls:
                yamls.remove(None)
                self.log_warning(
                    f"Template {template} contains empty or incorrect directives in the YAML file. They will be skipped from the template.")

            for yaml_doc in yamls:
                yaml_doc['metadata'].setdefault('labels', {}).update(labels)

        with open(template, 'w') as template_file:
            yaml.dump_all(yamls, template_file)

    def handle_apply(self, resource_path=None):
        if not resource_path:
            resource_path = self.get_variable('RESOURCE_PATH')

        if not resource_path:
            self.fail('Path to the spec file is required when running the "apply" command')

        labels = dict(self.get_variable('LABELS'))
        if self.get_variable('WITH_DEFAULT_LABELS'):
            bitbucket_branch = os.getenv('BITBUCKET_BRANCH')
            if bitbucket_branch is not None and '/' in bitbucket_branch:
                self.log_warning(
                    '"/" is not allowed in kubernetes labels. Slashes will be replaced by a dash "-" in the "bitbucket.org/bitbucket_branch" label value.'
                    'For more information you can check the official kubernetes docs'
                    'https://kubernetes.io/docs/concepts/overview/working-with-objects/labels/#syntax-and-character-set')
                labels['bitbucket.org/bitbucket_branch'] = bitbucket_branch.replace('/', '-')

            step_triggerer_uuid = os.getenv('BITBUCKET_STEP_TRIGGERER_UUID')
            if step_triggerer_uuid is not None:
                # trim the starting and closing curly braces
                labels['bitbucket.org/bitbucket_step_triggerer_uuid'] = step_triggerer_uuid[1:-1]

            variables = [
                ('BITBUCKET_COMMIT', 'bitbucket.org/bitbucket_commit'),
                ('BITBUCKET_REPO_OWNER', 'bitbucket.org/bitbucket_repo_owner'),
                ('BITBUCKET_REPO_SLUG', 'bitbucket.org/bitbucket_repo_slug'),
                ('BITBUCKET_BUILD_NUMBER', 'bitbucket.org/bitbucket_build_number'),
                ('BITBUCKET_DEPLOYMENT_ENVIRONMENT', 'bitbucket.org/bitbucket_deployment_environment')
            ]

            for variable, label_name in variables:
                value = os.getenv(variable)
                if value is not None:
                    labels[label_name] = value

        is_path_dir = os.path.isdir(resource_path)

        if is_path_dir:
            templates = [template for template in os.listdir(resource_path)
                         if template.endswith(('.yml', '.yaml')) and not
                         template.endswith(('kustomization.yml', 'kustomization.yaml'))]
            templates_absolute_paths = [os.path.join(resource_path, template_file) for template_file in templates]
        else:
            templates_absolute_paths = [resource_path]

        for template_file in templates_absolute_paths:
            self.update_constants(template_file)

            self.update_labels_in_metadata(template_file, labels)

        debug = ['--v=2'] if self.get_variable('DEBUG') else []

        result = subprocess.run(['kubectl', 'apply', self.get_variable('KUBECTL_APPLY_ARGS'), resource_path] + list(
            self.get_variable('KUBECTL_ARGS')) + debug, stdout=sys.stdout)

        if result.returncode != 0:
            self.fail('kubectl apply failed.')
        else:
            self.success('kubectl apply was successful.')

    def handle_delete(self, resource_path=None):
        if not resource_path:
            resource_path = self.get_variable('RESOURCE_PATH')

        if not resource_path:
            self.fail('Path to the spec file is required when running the "delete" command')

        is_path_dir = os.path.isdir(resource_path)

        if is_path_dir:
            templates = [template for template in os.listdir(resource_path)
                         if template.endswith(('.yml', '.yaml')) and not
                         template.endswith(('kustomization.yml', 'kustomization.yaml'))]
            templates_absolute_paths = [os.path.join(resource_path, template_file) for template_file in templates]
        else:
            templates_absolute_paths = [resource_path]

        for template_file in templates_absolute_paths:
            self.update_constants(template_file)

        debug = ['--v=2'] if self.get_variable('DEBUG') else []

        result = subprocess.run(['kubectl', 'delete', self.get_variable('KUBECTL_APPLY_ARGS'), resource_path] + list(
            self.get_variable('KUBECTL_ARGS')) + debug, stdout=sys.stdout)

        if result.returncode != 0:
            self.fail('kubectl delete failed.')
        else:
            self.success('kubectl delete was successful.')

    def handle_deploy_web(self):
        k8s_env = self.get_variable('K8s_ENV')

        # Create Deployment
        self.handle_apply(f'k8s/{k8s_env}/deployment.yml')

        # Create Service
        self.handle_apply(f'k8s/{k8s_env}/service.yml')

        ingress_name = self.get_variable('INGRESS_NAME')
        if ingress_name is not None:
            domain_name = self.get_variable('DOMAIN_NAME')
            service_name = self.get_variable('SERVICE_NAME')

            if domain_name is None or service_name is None:
                self.fail('"DOMAIN_NAME" and "SERVICE_NAME" is required when updating ingress')

            script = os.path.join(self._script_path, 'update-ingress.sh')
            cmd = f'{script} {domain_name} {ingress_name} {service_name}'.split()
            subprocess.run(cmd, stdout=sys.stdout)

    def handle_generic(self, cmd):
        debug = ['--v=2'] if self.get_variable('DEBUG') else []
        result = subprocess.run(['kubectl'] + cmd.split() + list(self.get_variable('KUBECTL_ARGS')) + debug,
                                stdout=sys.stdout)

        if result.returncode != 0:
            self.fail(f'kubectl {cmd} failed.')
        else:
            self.success(f'kubectl {cmd} was successful.')

    def handle_remove_web(self):
        k8s_env = self.get_variable('K8s_ENV')

        ingress_name = self.get_variable('INGRESS_NAME')
        if ingress_name is not None:
            domain_name = self.get_variable('DOMAIN_NAME')

            if domain_name is None:
                self.fail('"DOMAIN_NAME" is required when removing ingress')

            script = os.path.join(self._script_path, 'remove-ingress.sh')
            cmd = f'{script} {domain_name} {ingress_name}'.split()
            subprocess.run(cmd, stdout=sys.stdout)

        # Create Deployment
        self.handle_delete(f'k8s/{k8s_env}/deployment.yml')

        # Create Service
        self.handle_delete(f'k8s/{k8s_env}/service.yml')

    def handle_deploy_crons(self):
        k8s_env = self.get_variable('K8s_ENV')

        # Handler Cron Deployment
        self.handle_apply(f'k8s/{k8s_env}/crons')

    def handle_deploy_jobs(self):

        k8s_env = self.get_variable('K8s_ENV')

        # Handler Jobs Deployment
        self.handle_delete(f'k8s/{k8s_env}/jobs')

        self.handle_apply(f'k8s/{k8s_env}/jobs')

    def run(self):
        self.configure()

        cmd = self.get_variable('KUBECTL_COMMAND')
        if cmd == 'deploy_web':
            self.handle_deploy_web()
        elif cmd == 'remove_web':
            self.handle_remove_web()
        elif cmd == 'deploy_jobs':
            self.handle_deploy_jobs()
        elif cmd == 'deploy_crons':
            self.handle_deploy_crons()
        elif cmd == 'apply':
            self.handle_apply()
        else:
            self.handle_generic(cmd)

        self.success(message=f"Pipe finished successfully!")


if __name__ == '__main__':
    pipe = EKSDeployPipe(schema=schema, check_for_newer_version=True)
    pipe.run()
